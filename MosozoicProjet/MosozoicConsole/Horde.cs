﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MosozoicConsole
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs;
        public Horde()
        {
            this.dinosaurs = new List<Dinosaur>();
        }

        public void AddDinosaur(Dinosaur dino)
        {
            this.dinosaurs.Add(dino);
        }

        public void RemoveDinosaur(Dinosaur dino)
        {
            this.dinosaurs.Remove(dino);
        }

        public string IntroduceAll()
        {
            StringBuilder introduce_builder = new StringBuilder();
            foreach (Dinosaur dinosaur in this.dinosaurs)
            {
                introduce_builder.AppendFormat("{0}\n", dinosaur.sayHello());
            }
            return introduce_builder.ToString().Trim();
        }

        public List<Dinosaur> GetDinosaurs()
        {
            return this.dinosaurs;
        }
    }
}
