﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MosozoicConsole;

namespace MosozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void TestAddDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.IsEmpty(horde.GetDinosaurs());
            horde.AddDinosaur(louis);
            Assert.IsSingle(horde.GetDinosaurs());
            Assert.AreEqual(louis, horde.GetDinosaurs()[0]);
            horde.AddDinosaur(nessie);
            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            Assert.AreEqual(nessie, horde.GetDinosaurs()[1]);
        }

        [TestMethod]
        public void TestRemoveDinosaur()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            horde.RemoveDinosaur(louis);
            Assert.IsSingle(horde.GetDinosaurs());
            horde.RemoveDinosaur(nessie);
            Assert.IsEmpty(horde.GetDinosaurs());
        }


        [TestMethod]
        public void TestIntroduceAll()
        {
            Horde horde = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur bob = new Dinosaur("Bob", "Raptor Jesus", 24);
            Dinosaur sully = new Dinosaur("Sully", "T-rex", 7);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);
            horde.AddDinosaur(bob);
            horde.AddDinosaur(sully);

            string expected_introduction = "Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\nJe suis Bob le Raptor Jesus, j'ai 24 ans.\nJe suis Sully le T-rex, j'ai 7 ans.\n";

            Assert.AreEqual(expected_introduction, horde.IntroduceAll());
        }
    }
}
